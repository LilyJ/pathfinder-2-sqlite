ability_mods:
  cha_mod: 5
  con_mod: 4
  dex_mod: 7
  int_mod: -3
  str_mod: 5
  wis_mod: 1
ac: 33
ac_special: null
alignment: NE
automatic_abilities: null
description: 'These batlike monstrosities inhabit the nooks and crevices of the Darklands,
  where they hang from stalactites and swoop down on unsuspecting prey. Though they
  are completely sightless, deculis possess rudimentary nerves in their skulls that
  allow them to detect the presence of infrared light—information they use to locate
  potential food and threats. They are deadly hunters, thanks largely to their magical
  ability to manipulate the ebb and flow of shadows. Deculis prefer to feed upon the
  warm blood of freshly killed prey, but they can subsist on carrion if they have
  to.




  **__Recall Knowledge - Beast__ (__Arcana__, __Nature__)**: DC 30'
hp: 215
hp_misc: null
immunities:
- visual
items: null
languages:
- Undercommon
- can't speak any language
level: 12
melee:
- action_cost: One Action
  damage:
    formula: 3d12+11
    type: piercing
  name: fangs
  plus_damage: null
  to_hit: 24
  traits: null
- action_cost: One Action
  damage:
    formula: 3d8+11
    type: bludgeoning
  name: wing
  plus_damage: null
  to_hit: 26
  traits:
  - agile
  - finesse
name: Deculi
perception: 23
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: null
  effect: 'The deculi creates an extradimensional space adjacent to itself in any
    10-foot-by-10-foot area of darkness and enters it. To non-deculis, the extradimensional
    space looks like an ordinary area of darkness (though effects such as __true seeing__
    can see through this illusion). The deculi''s infrared vision functions normally
    between this extradimensional space and the area outside its confines, allowing
    the deculi to sense what is going on around it, and its innate __darkness__ spell
    also extends to the area around the shadow sanctuary. Attacks cannot be made into
    or from the extradimensional space.


    The deculi can Dismiss this effect to emerge from its shadow sanctuary, but the
    space it emerges into must be in darkness and must be unobstructed. If the area
    is obstructed, the deculi is shunted to the nearest available space and takes
    1d6 bludgeoning damage per 5 feet shunted. If the area overlaying the shadow sanctuary
    becomes illuminated by magical light, the extradimensional space can no longer
    be accessed and the deculi is trapped within until the area is once again in darkness.
    The deculi can try to extinguish a magical light effect obscuring its sanctuary
    once per day by attempting to counteract the light effect with its __darkness__
    innate spell.


    If a deculi dies (including from hunger, thirst, or asphyxiation) while within
    the extradimensional space, the shadow sanctuary immediately ends and the deculi''s
    corpse is expelled. If the area is illuminated, the corpse is not expelled until
    the sanctuary entrance is in darkness once again.'
  effects: null
  failure: null
  frequency: once per day
  full_description: null
  generic_description: null
  name: Create Shadow Sanctuary
  range: null
  raw_description: '**Create Shadow Sanctuary** [Two Actions]  (__conjuration__, __darkness__,
    __extradimensional__, __occult__) **Frequency** once per day; **Effect** The deculi
    creates an extradimensional space adjacent to itself in any 10-foot-by-10-foot
    area of darkness and enters it. To non-deculis, the extradimensional space looks
    like an ordinary area of darkness (though effects such as __true seeing__ can
    see through this illusion). The deculi''s infrared vision functions normally between
    this extradimensional space and the area outside its confines, allowing the deculi
    to sense what is going on around it, and its innate __darkness__ spell also extends
    to the area around the shadow sanctuary. Attacks cannot be made into or from the
    extradimensional space.


    The deculi can Dismiss this effect to emerge from its shadow sanctuary, but the
    space it emerges into must be in darkness and must be unobstructed. If the area
    is obstructed, the deculi is shunted to the nearest available space and takes
    1d6 bludgeoning damage per 5 feet shunted. If the area overlaying the shadow sanctuary
    becomes illuminated by magical light, the extradimensional space can no longer
    be accessed and the deculi is trapped within until the area is once again in darkness.
    The deculi can try to extinguish a magical light effect obscuring its sanctuary
    once per day by attempting to counteract the light effect with its __darkness__
    innate spell.


    If a deculi dies (including from hunger, thirst, or asphyxiation) while within
    the extradimensional space, the shadow sanctuary immediately ends and the deculi''s
    corpse is expelled. If the area is illuminated, the corpse is not expelled until
    the sanctuary entrance is in darkness once again.'
  requirements: null
  success: null
  traits:
  - conjuration
  - darkness
  - extradimensional
  - occult
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: null
  effect: The deculi is in its shadow sanctuary.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Shadow Strike
  range: null
  raw_description: '**Shadow Strike** [Two Actions]  **Requirements** The deculi is
    in its shadow sanctuary. **Effect** The deculi Dismisses its shadow sanctuary.
    It Strides, Climbs, or Flies up to its Speed, then makes a fangs Strike that deals
    an additional 4d6 precision damage.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the deculi is Hiding in darkness and Sneaks, it can move up to
    its full Speed instead of half its Speed.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Walk in Shadow
  range: null
  raw_description: '**Walk in Shadow** When the deculi is Hiding in darkness and Sneaks,
    it can move up to its full Speed instead of half its Speed.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 21
  fort_misc: null
  misc: null
  ref: 26
  ref_misc: null
  will: 18
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A deculi can detect infrared radiation as a precise sense.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Infrared Vision
  range: null
  raw_description: '** Infrared Vision** A deculi can detect infrared radiation as
    a precise sense.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +23
- infrared vision 60 feet
- no vision
size: Large
skills:
- bonus: 24
  misc: null
  name: 'Acrobatics '
- bonus: 24
  misc: null
  name: 'Athletics '
- bonus: 26
  misc: null
  name: 'Stealth '
source:
- abbr: 'Pathfinder #148: Fires of the Haunted City'
  page_start: 78
  page_stop: null
speed:
- amount: 30
  type: Land
- amount: 30
  type: climb
- amount: 50
  type: fly
- amount: null
  type: walk in shadow
spell_lists:
- dc: 32
  misc: null
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 6
    spells:
    - frequency: at will
      name: darkness
      requirement: null
  to_hit: null
traits:
- NE
- Large
- Beast
type: Creature
weaknesses: null
